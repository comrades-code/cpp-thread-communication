#include "PFBackgroundService.h"
#include "SocketBackgroundService.h"
#include <iostream>

int main()
{
    //make an instance of the promise/future implementation 
    std::unique_ptr<IBackgroundService> pfService = std::make_unique<PFBackgroundService>(
        [](std::string msg)
        {
            std::cout << "pfService processing : " << msg << std::endl;
        });

    //make an instance of the socket implementation
    std::unique_ptr<IBackgroundService> socketService = std::make_unique<SocketBackgroundService>(
        [](std::string msg)
        {
            std::cout << "socketService processing : " << msg << std::endl;
        });

    //use both implementations
    std::string msg;
    for (int i = 0; i < 3; i++)
    {
        msg = "message " + std::to_string(i);
        pfService->processMessage(msg);
        socketService->processMessage(msg);
    }

    return 0;
}