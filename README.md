# A comparison of two approaches to inter-thread communication in C++ : futures/promises vs. messaging over sockets.

This is prototype code from the following blog [post](https://blog.pramp.com/inter-thread-communication-in-c-futures-promises-vs-sockets-aeebfffd2107)

* `make all` (Uses g++)
* `./main`
* Note that the ZeroMQ lib (`libzmq.so`) was checked in for simplicity. You can find it [here](https://github.com/zeromq/libzmq)
