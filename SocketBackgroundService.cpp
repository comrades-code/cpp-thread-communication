#include "SocketBackgroundService.h"
#include "zhelpers.hpp"

const std::string SocketBackgroundService::STOP = "stop";

SocketBackgroundService::SocketBackgroundService(const std::function<void(std::string)>& reactor)
{
    m_context = std::make_unique<zmq::context_t>(1);
    m_manager = std::make_unique<zmq::socket_t>(*m_context, ZMQ_PAIR);
    m_manager->bind("tcp://*:5555");
    zmq::context_t& context = *m_context;
    m_thread = std::make_unique<std::thread>(
        [=, &context](std::function<void(std::string)> reactor)
        {
            //setup socket for PAIR communication with parent thread
            zmq::socket_t comm(context, ZMQ_PAIR);
            comm.connect("tcp://localhost:5555");

            //Initialize poll set
            zmq::pollitem_t pollSet[] = {
                { (void*)comm, 0, ZMQ_POLLIN, 0 }
            };

            std::string msg;
            while (true)
            {
                // poll the pollset immediately (timeout=0)
                zmq::poll(&pollSet[0], sizeof(pollSet) / sizeof(pollSet[0]), 0);
                //check the parent PAIR communication socket for updates
                if (pollSet[0].revents & ZMQ_POLLIN)
                {
                    //receive and process message
                    msg = s_recv(comm);
                    if (msg == SocketBackgroundService::STOP)
                    {
                        break;
                    }
                    reactor(msg);
                }
            }            
        }, reactor); 
}

void SocketBackgroundService::processMessage(std::string msg)
{
    bool success = false;
    while (!success)
    {
        success = s_send(*m_manager, msg);
    }
}

SocketBackgroundService::~SocketBackgroundService()
{
    processMessage(SocketBackgroundService::STOP);
    if (m_thread->joinable())
    {
        m_thread->join();
    }
}