#pragma once
#include "IBackgroundService.h"
#include <memory>
#include <thread>
#include <mutex>
#include "zmq.hpp"

class SocketBackgroundService : public IBackgroundService
{
private:
    //zmq context (necessary for sockets to work)
    std::unique_ptr<zmq::context_t> m_context;

    //socket that will talk to managed background thread's
    //socket
    std::unique_ptr<zmq::socket_t> m_manager;
    
    //managed thread
    std::unique_ptr<std::thread> m_thread;

    //stop message
    const static std::string STOP;
public:
    //constructor (takes in fptr to a "reactor" function that "reacts"
    //to a string message)
    SocketBackgroundService(const std::function<void(std::string)>& reactor);
    
    //interface method
    void processMessage(std::string msg);

    //destructor (stops and waits for managed thread to exit)
    ~SocketBackgroundService();
};