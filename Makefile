#Macros
CC = g++
CFLAGS = -std=c++14 -pthread

#---------------------------------------------------------------------
# Build rules for non-file targets
#---------------------------------------------------------------------

all: main

clean:
	rm -f main
	rm -f *.o

#---------------------------------------------------------------------
# Build rules for everything else
#---------------------------------------------------------------------

main: PFBackgroundService.o main.o SocketBackgroundService.o libzmq.so
	$(CC) $(CFLAGS) PFBackgroundService.o main.o SocketBackgroundService.o libzmq.so -o main

PFBackgroundService.o: PFBackgroundService.h IBackgroundService.h
	$(CC) $(CFLAGS) -c PFBackgroundService.cpp

SocketBackgroundService.o: SocketBackgroundService.h IBackgroundService.h
	$(CC) $(CFLAGS) -c SocketBackgroundService.cpp

main.o: main.cpp PFBackgroundService.h SocketBackgroundService.h IBackgroundService.h
	$(CC) $(CFLAGS) -c main.cpp